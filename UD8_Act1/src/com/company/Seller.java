package com.company;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Seller {

    private AbstractZone[] zone;

    private ArrayList<Ticket> soldTickets;

    private Scanner sc;

    private int ticketIdentifier;

    private AbstractZone ticketZone;

    private String ticketName;

    Seller() {

        this.zone = new AbstractZone[4];
        this.zone[0] = new Principal();
        this.zone[1] = new Box();
        this.zone[2] = new Central();
        this.zone[3] = new Lateral();
        this.soldTickets = new ArrayList<>();
        this.sc = new Scanner(System.in);
        this.ticketIdentifier = 0;
    }

    public void launcher() {

        boolean error;

        do {

            try {

                System.out.println("1- Vender entrada.");

                System.out.println("2- Consultar entrada.");

                System.out.println("3- Informar zona.");

                switch (sc.nextInt()) {

                    case 1:

                        sellTicket();

                        break;

                    case 2:

                        showTicket();

                        break;

                    case 3:

                        showZone();

                        break;
                }

                error = false;

            } catch (InputMismatchException e) {

                System.out.println("Número inválido\n");

                this.sc.next();

                error = true;
            }

        } while (error);
    }

    private void showTicket() {

        int identifier = 0;

        boolean error;

        do {

            try {

                System.out.println("Introduzca el identificador de la entrada:");

                identifier = this.sc.nextInt();

                error = false;

            } catch (InputMismatchException e) {

                System.out.println("Número inválido\n");

                this.sc.next();

                error = true;
            }

        } while(error);

        for (int i = 0; i <= this.soldTickets.size(); i++) {

            if (i == this.soldTickets.size()) {

                System.out.println("El identificador introducido no se corresponde con ninguna entrada.");

                if (otherIdentifier()) {

                    showTicket();
                }

                if (goBack()) {

                    launcher();
                }

            } else if (identifier == this.soldTickets.get(i).getIdentifier()) {

                System.out.println("Datos de la entrada:\n");

                System.out.println("Identificador => " + this.soldTickets.get(i).getIdentifier());
                System.out.println("Zona => " + this.soldTickets.get(i).getZone().getNameZone());
                System.out.println("Nombre del comprador => " + this.soldTickets.get(i).getBuyerName());

                if (goBack()) {

                    launcher();
                }
            }
        }
    }

    private void showZone() {

        boolean error;

        do {

            try {

        System.out.println("¿De que zona desea informacion?\n");

        System.out.println("1- Principal");
        System.out.println("2- Palco");
        System.out.println("3- Central");
        System.out.println("4- Lateral");



                switch (sc.nextInt()) {

                    case 1:

                        System.out.println("Datos de la zona:\n");

                        System.out.printf("%d localidades vendidas.\n", this.zone[0].soldTickets());
                        System.out.printf("%.2f € recaudados.\n", this.zone[0].getCollection());

                        break;

                    case 2:

                        System.out.println("Datos de la zona:\n");

                        System.out.printf("%d localidades vendidas.\n", this.zone[1].soldTickets());
                        System.out.printf("%.2f € recaudados.\n", this.zone[1].getCollection());

                        break;

                    case 3:

                        System.out.println("Datos de la zona:\n");

                        System.out.printf("%d localidades vendidas.\n", this.zone[2].soldTickets());
                        System.out.printf("%.2f € recaudados.\n", this.zone[2].getCollection());

                        break;

                    case 4:

                        System.out.println("Datos de la zona:\n");

                        System.out.printf("%d localidades vendidas.\n", this.zone[3].soldTickets());
                        System.out.printf("%.2f € recaudados.\n", this.zone[3].getCollection());

                        break;

                    default:

                        System.out.println("Escribe un número válido.\n");

                        showZone();
                }

                error = false;

            } catch (InputMismatchException e) {

                System.out.println("Número inválido.\n");

                this.sc.next();

                error = true;
            }

        } while(error);

        if (goBack()) {

            launcher();
        }
    }

    private void sellTicket() {

        this.ticketZone = chooseZone();

        if (this.ticketZone.isFull()) {

            System.out.println("La zona elegida está completa.\n");

            if (goBack()) {

                launcher();
            }

        } else {

            chooseTicketType();

            this.ticketName = chooseName();

            this.ticketIdentifier = generateIdentifier();

            this.soldTickets.add(createTicket());

            System.out.printf("Identificador de la entrada -> %d\n"
                    , this.soldTickets.get(this.soldTickets.size()-1).getIdentifier());

            System.out.printf("Precio de la entrada -> %.2f €\n"
                    , this.soldTickets.get(this.soldTickets.size()-1).getZone().getLastPrice());

            if (goBack()) {

                launcher();
            }
        }
    }

    private Ticket createTicket() {

        return new Ticket(this.ticketIdentifier, this.ticketZone, this.ticketName);
    }

    private void chooseTicketType() {

        boolean error;

        do {

            try {

                System.out.println("¿Que tipo de entrada quiere comprar?\n");

                System.out.printf("1- Normal (%.2f €)\n", this.ticketZone.getNormalPrice());
                System.out.printf("2- Reducida (%.2f €)\n", this.ticketZone.getReducedPrice());
                System.out.printf("3- Abonada (%.2f €)\n", this.ticketZone.getImprovedPrice());

                switch (this.sc.nextInt()) {

                    case 1:

                        for (int i = 0; i < this.zone.length; i++) {

                            if (this.ticketZone.equals(this.zone[i])) {

                                this.zone[i].addNormalCollection();

                                this.zone[i].substractLocality();
                            }
                        }

                        break;

                    case 2:

                        for (int i = 0; i < this.zone.length; i++) {

                            if (this.ticketZone.equals(this.zone[i])) {

                                this.zone[i].addReducedCollection();

                                this.zone[i].substractLocality();
                            }
                        }

                        break;

                    case 3:

                        for (int i = 0; i < this.zone.length; i++) {

                            if (this.ticketZone.equals(this.zone[i])) {

                                this.zone[i].addImprovedCollection();

                                this.zone[i].substractLocality();
                            }
                        }

                        break;

                    default:

                        System.out.println("Escribe un número válido.\n");

                        chooseTicketType();

                        break;
                }

                error = false;

            } catch (InputMismatchException e) {

                System.out.println("Número inválido.\n");

                this.sc.next();

                error = true;
            }

        } while (error);
    }

    private AbstractZone chooseZone() {

        int number = 0;

        boolean error;

        do {

            try {

                System.out.println("Elige una zona:\n");

                System.out.println("1- Principal");
                System.out.println("2- Palco");
                System.out.println("3- Central");
                System.out.println("4- Lateral");

                number = this.sc.nextInt();

                error = false;

            } catch (InputMismatchException e) {

                System.out.println("Número inválido.\n");

                this.sc.next();

                error = true;
            }

        } while (error);

        if (number == 1) {

            return this.zone[0];

        } else if (number == 2) {

            return this.zone[1];

        } else if (number == 3) {

            return this.zone[2];

        }else if (number == 4) {

            return this.zone[3];

        }

        System.out.println("Escribe un número válido.\n");

        return chooseZone();
    }

    private String chooseName() {

        System.out.println("Elige un nombre:");

        return this.sc.next();
    }

    private int generateIdentifier() {

        return this.ticketIdentifier + 1;
    }

    private boolean otherIdentifier() {

        System.out.println("¿Probar otro identificador? (s/n)");

        return this.sc.next().equals("s");
    }

    private boolean goBack() {

        System.out.println("¿Volvar atrás?(s/n)");

        return this.sc.next().equals("s");
    }
}
